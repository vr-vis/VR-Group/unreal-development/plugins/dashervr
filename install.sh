#!/bin/bash
mkdir Source/Thirdparty/Build
cd Source/Thirdparty/Build
if ! command -V cmake3 &> /dev/null
then
	if ! command -V cmake &> /dev/null
	then
		echo "cmake not found"
	else
		cmake ..
	fi
else
	cmake3 ..
fi
make install

