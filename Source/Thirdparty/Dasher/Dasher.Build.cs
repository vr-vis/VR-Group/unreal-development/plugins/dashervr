// Copyright Epic Games, Inc. All Rights Reserved.

using System;
using System.IO;
using UnrealBuildTool;

public class Dasher : ModuleRules
{
	public Dasher(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		CppStandard = CppStandardVersion.Cpp17;
		Type = ModuleType.External;

		PrivateDefinitions.Add("HAVE_OWN_FILEUTILS");
		PublicDefinitions.Add("HAVE_ROUND=1");


		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore"
            }
		);
		
		DynamicallyLoadedModuleNames.AddRange(new string[]{});

		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore"));
        PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "Common"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "Common", "Allocators"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "Common", "Platform"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "Common", "Unicode"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "DasherCore"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "DasherCore", "Alphabet"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Src", "DasherCore", "LanguageModelling"));
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "DasherCore", "Thirdparty", "pugixml", "src"));
		
		if (Target.Platform == UnrealTargetPlatform.Win64)
		{
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "Lib", "DasherCore.lib"));
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "Lib", "pugixml.lib"));
		}
		if (Target.Platform == UnrealTargetPlatform.Linux)
		{
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "Lib", "DasherCore.a"));
			PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "Lib", "libpugixml.a"));
		}
	}
}
