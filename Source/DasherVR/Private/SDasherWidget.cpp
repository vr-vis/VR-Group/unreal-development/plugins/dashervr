#include "SDasherWidget.h"
#include "SlateOptMacros.h"
#include "Styling/CoreStyle.h"
#include "Brushes/SlateColorBrush.h"
#include "Rendering/DrawElements.h"
#include "DasherInterface.h"

#include "Components/SlateWrapperTypes.h"
#include "Framework/Application/SlateApplication.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION


// ++ This is needed in order to use the localization macro LOCTEXT
#define LOCTEXT_NAMESPACE "SStandardSlateWidget"

//Set the state if we're in the editor or not.
void SDasherWidget::SetEditor(bool EditorState)
{
	IsEditor = EditorState;
}

void SDasherWidget::EnableInput(bool enable)
{
	InputPaused = !enable;
}

bool SDasherWidget::GetInputEnabled() const
{
	return !InputPaused;
}

//Event Handlers
//Mouse position saved for mouse Input
FReply SDasherWidget::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
    if (InputPaused) return FReply::Unhandled();

	FMouseState& MouseState = GetMouseState(MouseEvent.GetPointerIndex());

	if(MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton && !MouseState.PrimaryButtonPressed)
	{
		DasherMainInterface->KeyDown(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Primary_Input);
		MouseState.PrimaryButtonPressed = true;
		MouseListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), MouseState.PrimaryButtonPressed);
	}
	else if(MouseEvent.GetEffectingButton() == EKeys::RightMouseButton && !MouseState.SecondaryButtonPressed)
	{
		DasherMainInterface->KeyDown(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Secondary_Input);
		MouseState.SecondaryButtonPressed = true;
		BoostListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), MouseState.SecondaryButtonPressed);
	}

	UpdateMouseInteraction(MouseEvent.GetPointerIndex());
	return FReply::Handled();
}

FMouseState& SDasherWidget::GetMouseState(int Index)
{
    if(!MouseStates.Contains(Index)) MouseStates.Add(Index, {});
    return MouseStates[Index];
}

// Moves Item to Back
void SDasherWidget::UpdateMouseInteraction(int Index)
{
    MouseInteractions.RemoveSingle(Index);
	MouseInteractions.Add(Index);
}

int SDasherWidget::GetLastActiveMouseInteraction() const
{
	// Find Mouse that already interacted
    for(int i = MouseInteractions.Num() - 1; i >= 0; i--)
    {
        if(MouseStates[MouseInteractions[i]].CurrentlyHovering) return MouseInteractions[i];
    }

	//None found, thus take the first one that currently hovers
	for(const TPair<int, FMouseState>& Pair : MouseStates)
	{
	    if(Pair.Value.CurrentlyHovering) return Pair.Key;
	}

	return -1;
}

FReply SDasherWidget::OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
    if (InputPaused) return FReply::Unhandled();

	FMouseState& MouseState = GetMouseState(MouseEvent.GetPointerIndex());

	//The mouse event only contains the Screen Space Position
	MouseState.CursorPosition = MyGeometry.AbsoluteToLocal(MouseEvent.GetScreenSpacePosition());
	MouseState.CurrentlyHovering = true; //ensure hover set

	return FReply::Handled();
}

void SDasherWidget::OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	SWidget::OnMouseEnter(MyGeometry, MouseEvent);

	FMouseState& MouseState = GetMouseState(MouseEvent.GetPointerIndex());

	//Reenable if button is still pressed
	if(MouseState.DisabledDueToMouseLeave)
	{
	    if(MouseState.PrimaryButtonPressed && MouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton))
	    {
	        DasherMainInterface->KeyDown(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Primary_Input);
		    UpdateMouseInteraction(MouseEvent.GetPointerIndex());
		    MouseListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), true);
	    }
	    if(MouseState.SecondaryButtonPressed && MouseEvent.IsMouseButtonDown(EKeys::RightMouseButton))
	    {
	        DasherMainInterface->KeyDown(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Secondary_Input);
		    UpdateMouseInteraction(MouseEvent.GetPointerIndex());
		    BoostListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), true);
	    }
	    MouseState.DisabledDueToMouseLeave = false;
	}
}

void SDasherWidget::OnMouseLeave(const FPointerEvent& MouseEvent)
{
	SWidget::OnMouseLeave(MouseEvent);

	FMouseState& MouseState = GetMouseState(MouseEvent.GetPointerIndex());
	MouseState.CurrentlyHovering = false;

	if (InputPaused) return;

	if(MouseState.PrimaryButtonPressed)
	{
	    DasherMainInterface->KeyUp(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Primary_Input);
		MouseListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), false);
		MouseState.DisabledDueToMouseLeave = true;
	}

	if(MouseState.SecondaryButtonPressed)
	{
	    DasherMainInterface->KeyUp(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Secondary_Input);
		BoostListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), false);
		MouseState.DisabledDueToMouseLeave = true;
	}
}

FReply SDasherWidget::OnMouseButtonDoubleClick(const FGeometry& InMyGeometry, const FPointerEvent& InMouseEvent)
{
    return OnMouseButtonUp(InMyGeometry, InMouseEvent);
}

FReply SDasherWidget::OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
	FMouseState& MouseState = GetMouseState(MouseEvent.GetPointerIndex());

	if(MouseEvent.GetEffectingButton() == EKeys::LeftMouseButton && MouseState.PrimaryButtonPressed)
	{
		DasherMainInterface->KeyUp(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Primary_Input);
		MouseState.PrimaryButtonPressed = false;
		MouseListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), MouseState.PrimaryButtonPressed);
	}
	else if(MouseEvent.GetEffectingButton() == EKeys::RightMouseButton && MouseState.SecondaryButtonPressed)
	{
		DasherMainInterface->KeyUp(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Secondary_Input);
		MouseState.SecondaryButtonPressed = false;
		BoostListeners.ExecuteIfBound(MouseEvent.GetPointerIndex(), MouseState.SecondaryButtonPressed);
	}

	return FReply::Handled();
}

//Set in the HandleMouseMoveEvent or via Set InputVector
//See tick for geometry things that could be use to modify this.
FVector2D SDasherWidget::GetCursorPosition() const
{
	const int lastInteraction = GetLastActiveMouseInteraction();

	if(!MouseStates.Contains(lastInteraction)) return DasherMainInterface->ConvertDasher2Screen(Dasher::CDasherModel::ORIGIN_X, Dasher::CDasherModel::ORIGIN_Y); 

	return MouseStates[lastInteraction].CursorPosition;
}

bool SDasherWidget::GetScreenCoords(screenint& iX, screenint& iY, Dasher::CDasherView* pView)
{
	const FVector2D Position = GetCursorPosition();

	iX = Position.X;
	iY = Position.Y;

	return true;
}

void SDasherWidget::InputVector(const FVector2D InputVector)
{
	if (InputPaused) return;

	FMouseState& MouseState = GetMouseState(VectorInputPointerIndex);

	MouseState.CursorPosition = DasherMainInterface->ConvertDasher2Screen(Dasher::CDasherModel::ORIGIN_X, Dasher::CDasherModel::ORIGIN_Y)
		+ InputVector * FVector2D(1.0f, -1.0f) * (GetHeight() / 2);

	MouseState.CurrentlyHovering = InputVector.Size() > 0.01; //Deadzone
}

void SDasherWidget::VectorInputButton(bool Pressed)
{
	if (InputPaused) return;

	FMouseState& MouseState = GetMouseState(VectorInputPointerIndex);

	if (Pressed)
	{
		DasherMainInterface->KeyDown(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Primary_Input);
		MouseState.PrimaryButtonPressed = true;
	    UpdateMouseInteraction(VectorInputPointerIndex);
	}
	else
	{
		DasherMainInterface->KeyUp(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Primary_Input);
		MouseState.PrimaryButtonPressed = false;
	}
}

void SDasherWidget::VectorBoostButton(bool Pressed)
{
	if (InputPaused) return;

	FMouseState& MouseState = GetMouseState(VectorInputPointerIndex);

	if (Pressed)
	{
		DasherMainInterface->KeyDown(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Secondary_Input);
		MouseState.SecondaryButtonPressed = true;
	}
	else
	{
		DasherMainInterface->KeyUp(FDateTime::Now().GetSecond() + FDateTime::Now().GetMillisecond(), Dasher::Keys::Secondary_Input);
		MouseState.SecondaryButtonPressed = false;
	}

	UpdateMouseInteraction(VectorInputPointerIndex);
}

//The construction of the Dasher Widget, Dasher parameters are set here, a function to set them outside is not provided, but can be easily implemented
void SDasherWidget::Construct(const FArguments& InArgs)
{
	//Initial resize, needed for setup
	Width = InArgs._width;
	Height = InArgs._height;
	resize(Width, Height);

	//initialize the font measuring service.
	FontMeasureService = FSlateApplication::Get().GetRenderer()->GetFontMeasureService();

	//Setting up Dasher
	static Dasher::XMLErrorDisplay display;
	Dasher::XmlSettingsStore* Settings = new Dasher::XmlSettingsStore("Settings.xml", &display); //Gets deleted somewhere else
	Settings->Load();
	Settings->Save();
	DasherMainInterface = MakeShared<Dasher::DasherInterface>(Settings);
	DasherMainInterface->GetModuleManager()->RegisterInputDeviceModule(this, true);

	DasherMainInterface->SetScreen(this);
	DasherMainInterface->SetBuffer(0);

	DasherMainInterface->SetCharEnteredCallback([this](FString Char, FString Buffer)
	{
		CharacterEntered.ExecuteIfBound(Char, Buffer);
		CharEnteredInFrame = true;
		BufferAltered.ExecuteIfBound(Buffer);
	});
	DasherMainInterface->SetCharDeletedCallback([this](FString Char, FString Buffer)
	{
		CharacterDeleted.ExecuteIfBound(Char, Buffer);
		CharDeletedInFrame = true;
		BufferAltered.ExecuteIfBound(Buffer);
	});
}

void SDasherWidget::SetParameter(FString& ParameterName, bool Value)
{
	for (auto param = Dasher::Settings::parameter_defaults.begin(); param != Dasher::Settings::parameter_defaults.end(); ++param)
		if (param->second.name == TCHAR_TO_UTF8(*ParameterName))
		{
			DasherMainInterface->SetBoolParameter(param->first, Value);
			return;
		}
}

void SDasherWidget::SetParameter(FString& ParameterName, int64 Value)
{
	for (auto param = Dasher::Settings::parameter_defaults.begin(); param != Dasher::Settings::parameter_defaults.end(); ++param)
		if (param->second.name == TCHAR_TO_UTF8(*ParameterName))
		{
			DasherMainInterface->SetLongParameter(param->first, Value);
			return;
		}
}

void SDasherWidget::SetParameter(FString& ParameterName, FString Value)
{
	for (auto param = Dasher::Settings::parameter_defaults.begin(); param != Dasher::Settings::parameter_defaults.end(); ++param)
		if (param->second.name == TCHAR_TO_UTF8(*ParameterName))
		{
			DasherMainInterface->SetStringParameter(param->first, TCHAR_TO_UTF8(*Value));
			return;
		}
}

//paints our stuff, then lets compoundwidget (super::) draw its stuff
//This draws from bottom to top rectangles->lines->labels, this is enough for our use, but for more complex scenarios a proper layering system might have to be implemented
int32 SDasherWidget::OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	//this doesn't draw anything red, we just need a brush. Could probably find a better "empty" brush
	auto MyBrush = FSlateColorBrush(FColor::Red);

	FFilledRect* RectObject;
	FWriting* WritingObject;
	FPolyLine* LineObject;
	FVector2D Size;
	FString Text;
	FSlateFontInfo Font;

	TArray<TUniquePtr<DasherDrawGeometry>>& GeometryBuffer = *FrontBuffer;
	for (TUniquePtr<DasherDrawGeometry>& GeneralObject : GeometryBuffer)
	{
		switch (GeneralObject->Type)
		{
		case GeometryType::Rectangle:
			RectObject = static_cast<FFilledRect*>(GeneralObject.Get());
			Size = {FMath::Abs(RectObject->top.X - RectObject->bottom.X), FMath::Abs(RectObject->top.Y - RectObject->bottom.Y)};
			FSlateDrawElement::MakeBox(OutDrawElements, LayerId++, AllottedGeometry.ToPaintGeometry(Size, FSlateLayoutTransform(1, RectObject->top)), &MyBrush, ESlateDrawEffect::None, RectObject->color); //actually adds the box to the geometry
			break;
		case GeometryType::Writing:
			WritingObject = static_cast<FWriting*>(GeneralObject.Get());
			Text = FString(*WritingObject->label);

			Font = FCoreStyle::GetDefaultFontStyle("Roboto", WritingObject->size, FFontOutlineSettings::NoOutline); //get the font
			FSlateDrawElement::MakeText(OutDrawElements, LayerId++, AllottedGeometry.ToPaintGeometry(FSlateLayoutTransform(1, FVector2D(WritingObject->pos.X, WritingObject->pos.Y))), Text, Font, ESlateDrawEffect::None, WritingObject->color);
			break;
		case GeometryType::PolyLine:
			LineObject = static_cast<FPolyLine*>(GeneralObject.Get());
			FSlateDrawElement::MakeLines(OutDrawElements, LayerId++, AllottedGeometry.ToPaintGeometry(), LineObject->points, ESlateDrawEffect::None, LineObject->color, true, LineObject->linewidth);
			break;
		default: break;
		}
	}

	return SCompoundWidget::OnPaint(Args, AllottedGeometry, MyClippingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled); //call the parent onPaint
}

//The tick function, we tick dasher in it and update the screen size for dasher
void SDasherWidget::Tick(const FGeometry& AllotedGeometry, const double InCurrentTime, const float InDeltaTime)
{
	//don't tick in the editor
	if (!IsEditor)
	{
		SCompoundWidget::Tick(AllotedGeometry, InCurrentTime, InDeltaTime);

		DasherMainInterface->Tick(static_cast<unsigned long>(InCurrentTime * 1000.0)); //we need to provide ticks in milliseconds


		//This probably doesn't have to be done every tick, but it does not seem to have a huge hit on performance
		const FGeometry Geometry = GetTickSpaceGeometry();
		const FIntPoint AbsoluteSize = Geometry.Size.IntPoint();
		if (Width != AbsoluteSize.X || Height != AbsoluteSize.Y)
		{
			Width = AbsoluteSize.X;
			Height = AbsoluteSize.Y;
			resize(Width, Height);
			//tell dasher we resized the screen, but only if there's actually a screen
			if (Width && Height) DasherMainInterface->ScreenResized(this);
		};
	}
}

std::pair<SDasherWidget::screenint, SDasherWidget::screenint> SDasherWidget::TextSize(CDasherScreen::Label* Label, unsigned Size)
{
	const FSlateFontInfo Font = FCoreStyle::GetDefaultFontStyle("Roboto", Size, FFontOutlineSettings::NoOutline); //get the font
	const FVector2D TextSize = FontMeasureService->Measure(FString(UTF8_TO_TCHAR(Label->m_strText.c_str())), Font, 1); //get the real size of the text, using the fontmeasuring service
	return {static_cast<screenint>(TextSize.X), static_cast<screenint>(TextSize.Y)};
}

//Double Buffers are rotated here.
void SDasherWidget::Display()
{
	std::swap(FrontBuffer, BackBuffer);
	BackBuffer->Empty();

	if(CharEnteredInFrame && CharDeletedInFrame)
	{
		CharacterSwitchedLastFrame.ExecuteIfBound();
	}
	else if(CharEnteredInFrame)
	{
		CharacterEnteredLastFrame.ExecuteIfBound();
	}
	else if(CharDeletedInFrame)
	{
		CharacterDeletedLastFrame.ExecuteIfBound();
	}

	CharEnteredInFrame = false;
	CharDeletedInFrame = false;

	DasherFrameCompleted.ExecuteIfBound();
}

//Only for testing
#define s(X) static_cast<uint8>(FMath::Clamp(X*(((colour>= 10 && colour <= 36) || (colour>= 140 && colour <= 166)) ? brightness : 1.0f),0.0f,255.0f))

//Functions for Drawing
void SDasherWidget::DrawRectangle(Dasher::screenint x1, Dasher::screenint y1, Dasher::screenint x2, Dasher::screenint y2, const Dasher::ColorPalette::Color& color, const Dasher::ColorPalette::Color& outlineColor, int iThickness)
{
    if (outlineColor == Dasher::ColorPalette::noColor) iThickness = 0; // Draw till brim if no outline color is given

    if (color != Dasher::ColorPalette::noColor && !color.isFullyTransparent())
    {
        BackBuffer->Add(MakeUnique<FFilledRect>(FVector2D(x1 + iThickness, y1 + iThickness), FVector2D(x2 - iThickness, y2 - iThickness), FLinearColor(color.Red / 255.0f, color.Green / 255.0f, color.Blue / 255.0f, color.Alpha / 255.0f)));
    }

    if (iThickness && outlineColor != Dasher::ColorPalette::noColor && !outlineColor.isFullyTransparent())
    {
        const float hThickness = iThickness / 2.0f;
        const FVector2D CornerMin = FVector2D(x1 + hThickness, y1 + hThickness);
        const FVector2D CornerMax = FVector2D(x2 - hThickness, y2 - hThickness);

        const FLinearColor oColor = FLinearColor(outlineColor.Red / 255.0f, outlineColor.Green / 255.0f, outlineColor.Blue / 255.0f, outlineColor.Alpha / 255.0f);
        BackBuffer->Add(MakeUnique<FPolyLine>(TArray({ FVector2D(CornerMin.X, CornerMin.Y - hThickness),FVector2D(CornerMin.X, CornerMax.Y + hThickness) }), static_cast<float>(iThickness), oColor, false));
        BackBuffer->Add(MakeUnique<FPolyLine>(TArray({ FVector2D(CornerMin.X - hThickness, CornerMax.Y),FVector2D(CornerMax.X + hThickness, CornerMax.Y) }), static_cast<float>(iThickness), oColor, false));
        BackBuffer->Add(MakeUnique<FPolyLine>(TArray({ FVector2D(CornerMax.X, CornerMax.Y + hThickness),FVector2D(CornerMax.X, CornerMin.Y - hThickness) }), static_cast<float>(iThickness), oColor, false));
        BackBuffer->Add(MakeUnique<FPolyLine>(TArray({ FVector2D(CornerMax.X + hThickness, CornerMin.Y),FVector2D(CornerMin.X - hThickness, CornerMin.Y) }), static_cast<float>(iThickness), oColor, false));
    }
}

void SDasherWidget::DrawString(CDasherScreen::Label* lab, screenint x1, screenint y1, unsigned int iSize, const Dasher::ColorPalette::Color& color) {
    BackBuffer->Add(MakeUnique<FWriting>(UTF8_TO_TCHAR(lab->m_strText.c_str()), FVector2D(x1, y1), static_cast<int>(iSize), FLinearColor(color.Red / 255.0f, color.Green / 255.0f, color.Blue / 255.0f, color.Alpha / 255.0f)));
}

void SDasherWidget::Polyline(CDasherScreen::point* points, int number, int iwidth, const Dasher::ColorPalette::Color& color) {
    TArray<FVector2D> PointArray;
    for (int i = 0; i < number; i++) {
        FVector2D Point(points[i].x, points[i].y);
        PointArray.Add(Point);
    }

    BackBuffer->Add(MakeUnique<FPolyLine>(PointArray, static_cast<float>(iwidth), FLinearColor(color.Red / 255.0f, color.Green / 255.0f, color.Blue / 255.0f, color.Alpha / 255.0f), true));
}

//techincally polygons are just multiple polylines. Dasher doesn't actually draw polygons in our case.
void SDasherWidget::Polygon(CDasherScreen::point* points, int number, const Dasher::ColorPalette::Color& fillcolor, const Dasher::ColorPalette::Color& outlinecolor, int iwidth) {
    TArray<FVector2D> PointArray;
    for (int i = 0; i < number; i++) {
        FVector2D Point(points[i].x, points[i].y);
        PointArray.Add(Point);
    }
    PointArray.Add(FVector2D(points[0].x, points[0].y));

    BackBuffer->Add(MakeUnique<FPolyLine>(PointArray, static_cast<float>(iwidth), FLinearColor(outlinecolor.Red / 255.0f, outlinecolor.Green / 255.0f, outlinecolor.Blue / 255.0f, outlinecolor.Alpha / 255.0f), true));
}


//We pass through the contents of the dasher buffer
FString SDasherWidget::GetBuffer() const
{
	return DasherMainInterface->GetBuffer();
}

void SDasherWidget::ResetBuffer()
{
	DasherMainInterface->ResetBuffer();
}

void SDasherWidget::StartTraining(FString PathToTextFile)
{
	DasherMainInterface->ImportTrainingFile(TCHAR_TO_UTF8(*FPaths::ConvertRelativePathToFull(PathToTextFile)));
}


// ++ We need to undefine this namespace after we finish creating the Slate widget
#undef LOCTEXT_NAMESPACE

END_SLATE_FUNCTION_BUILD_OPTIMIZATION
