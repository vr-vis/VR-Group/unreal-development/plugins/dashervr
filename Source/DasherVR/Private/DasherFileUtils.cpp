#include "CoreMinimal.h"
#include "DasherCoreWrapper.h"
#include "HAL/FileManagerGeneric.h"
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "Internationalization/Regex.h"

namespace Dasher
{
	int FileUtils::GetFileSize(const std::string& strFileName)
	{
		return IFileManager::Get().FileSize(UTF8_TO_TCHAR(strFileName.c_str()));
	}

	//This doesn't actually Parse all files with the pattern, just the file with exactly the name strPattern in the Project directory
	void FileUtils::ScanFiles(AbstractParser* parser, const std::string& strPattern)
	{ 
		const TArray<FString> SearchPaths = {
			FPaths::ConvertRelativePathToFull(FPaths::ProjectDir()),
			FPaths::ConvertRelativePathToFull(FPaths::ProjectDir(), TEXT("DasherResources"))
		};

		for(FString SearchPath : SearchPaths){
			//Check for explicit file
			if(IFileManager::Get().FileExists(*FPaths::Combine(SearchPath, FString(strPattern.c_str()))))
			{
				parser->ParseFile(TCHAR_TO_UTF8(*FPaths::Combine(SearchPath, FString(strPattern.c_str()))), true);
				return;
			}

			//Search files
			TArray<FString> Filenames;
			IFileManager::Get().FindFiles(Filenames, *SearchPath);
			FString alteredPattern = FString(strPattern.c_str()).Replace(TEXT("*"), TEXT(".*"));

			for(FString Filename : Filenames)
			{
				if(FRegexMatcher(FRegexPattern(alteredPattern), Filename).FindNext())
				{
					parser->ParseFile(TCHAR_TO_UTF8(*FPaths::Combine(SearchPath, Filename)), true);
				}
			}
		}
	}

	bool FileUtils::WriteUserDataFile(const std::string& filename, const std::string& strNewText, bool append)
	{
		if (strNewText.length() == 0) return true;
		FString Path = FString(filename.c_str());
		if(FPaths::IsRelative(Path)) Path = UTF8_TO_TCHAR(GetFullFilenamePath(filename).c_str());
		return FFileHelper::SaveStringToFile(FString(strNewText.c_str()), *Path, FFileHelper::EEncodingOptions::AutoDetect, &IFileManager::Get(), (append) ? FILEWRITE_Append : FILEWRITE_None);
	}
	
	std::string FileUtils::GetFullFilenamePath(const std::string strFilename){
		FString Fullpath = FPaths::ConvertRelativePathToFull(FPaths::ProjectDir(), UTF8_TO_TCHAR(strFilename.c_str()));
		return TCHAR_TO_UTF8(*Fullpath);
	}
}
