#include "DasherInterface.h"

#include "SDasherWidget.h"

namespace Dasher
{
	//Constructor
	DasherInterface::DasherInterface(CSettingsStore * settings) : CDashIntfScreenMsgs(settings){}

	void DasherInterface::ImportTrainingFile(std::string filename)
	{
		ImportTrainingText(filename);
	}

	FVector2D DasherInterface::ConvertDasher2Screen(myint InputX, myint InputY)
	{
		Dasher::screenint x, y;
		GetView()->Dasher2Screen(InputX, InputY,x,y);
		return FVector2D(x, y);
	}

	std::string DasherInterface::GetContext(unsigned int iStart, unsigned int iLength)
	{
		const FString output = Buffer.Mid(iStart, iLength);
		return TCHAR_TO_UTF8(*output);
	}

	//The next functions operate on the buffer

	//For now can only return one character around the cursor
	std::string DasherInterface::GetTextAroundCursor(Dasher::EditDistance iDist)
	{		
		if (Buffer.Len()>Cursor)
		{
			if (iDist == Dasher::EditDistance::EDIT_CHAR)
			{
				const FString Output = Buffer.Mid(Cursor, 1);
				return TCHAR_TO_UTF8(*Output);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("This is not Implemented"));
				return std::string("tried to get more than just a char");
			}
		}
		UE_LOG(LogTemp, Error, TEXT("Cursor out of bounds"));
		return std::string("Cursor out of bounds");
	}

	unsigned int DasherInterface::ctrlMove(bool bForwards, Dasher::EditDistance dist)
	{
		if (dist == Dasher::EditDistance::EDIT_CHAR)
		{
			if (bForwards) Cursor++;
			else Cursor--;
		}
		return Cursor;
	}

	unsigned int DasherInterface::ctrlDelete(bool bForwards, Dasher::EditDistance dist)
	{
		if (dist == Dasher::EditDistance::EDIT_CHAR)
		{
			const int Index = Cursor - (bForwards ? 0 : 1);
			const FString DeletedChar = Buffer.Mid(Index, 1);
			Buffer.RemoveAt(Index, 1, false);
			if(!bForwards) Cursor--;
			if(CharacterDeleted)
			{
				CharacterDeleted(DeletedChar, Buffer);
			}
		}
		return Cursor;
	}

	void DasherInterface::editOutput(const std::string& strText, CDasherNode* pNode)
	{
		if (static_cast<int>(Buffer.GetAllocatedSize()) - static_cast<int>(strText.length()) - Buffer.Len() < 0)
		{
			Buffer.Reserve(Buffer.GetAllocatedSize() * 2);
		}

		const FString Text(UTF8_TO_TCHAR(strText.c_str()));
		for(TCHAR StringChar : Text)
		{
			Buffer.AppendChar(StringChar);
			Cursor++;

			// Broadcast each character
			if(CharacterEntered)
			{
				CharacterEntered(FString(1, &StringChar), Buffer);
			}
		}

		CDasherInterfaceBase::editOutput(strText, pNode);
	}


	std::string DasherInterface::GetAllContext()
	{
		const FString Output = Buffer;
		Buffer.Empty();
		return std::string(TCHAR_TO_UTF8(*Output));
	}


	void DasherInterface::editDelete(const std::string& strText, CDasherNode* pNode)
	{
		const FString Text(UTF8_TO_TCHAR(strText.c_str()));

		//Reverse Iterate
		for(TCHAR StringChar : Text.Reverse())
		{
			const FString SingleCharString = FString(1, &StringChar);
			if(Buffer.RemoveFromEnd(SingleCharString))
			{
				Cursor--;
				if(CharacterDeleted) CharacterDeleted(SingleCharString, Buffer);
			}
		}

		CDasherInterfaceBase::editDelete(strText, pNode);
	}


	void DasherInterface::Tick(unsigned long time)
	{
		NewFrame(time, true);
	}

	void DasherInterface::SetScreen(CDasherScreen* screen)
	{
		//set the widget as screen that dasher will be displayed on
		ChangeScreen(screen);
		//set up stuff that can't be done in the constructor (for explanation see comments in DasherInterfaceBase.h etc. in DasherCore)
		Realize(0);
	}

	FString DasherInterface::GetBuffer() const
    {
		return Buffer;
	}

	void DasherInterface::ResetBuffer()
	{
		Buffer = "";
		SetBuffer(0);
	}
		
}
