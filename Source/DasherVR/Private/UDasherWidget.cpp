/*
  Adapted from JoySoftEdgeImage by Rama https://nerivec.github.io/old-ue4-wiki/pages/umg-custom-widget-components-and-render-code-usable-in-umg-designer.html
*/
#include "UDasherWidget.h"

//LOCTEXT
#define LOCTEXT_NAMESPACE "UMG"

/////////////////////////////////////////////////////
// UDasherWidget
UDasherWidget::UDasherWidget(const FObjectInitializer& ObjectInitializer)
  : Super(ObjectInitializer)
{
	//Default Values Set Here, see above
}

//Rebuild using custom Slate Widget 
TSharedRef<SWidget> UDasherWidget::RebuildWidget()
{
	if (!DasherScreen)
	{
		DasherScreen = SNew(SDasherWidget).height(1080).width(1920);
	}
	return DasherScreen.ToSharedRef();
}

void UDasherWidget::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	//Check if we're in Editor
	DasherScreen->SetEditor(IsDesignTime());
	DasherScreen->CharacterEntered.BindLambda([this](FString Char, FString Buffer){CharacterEntered.Broadcast(Char, Buffer); });
	DasherScreen->CharacterDeleted.BindLambda([this](FString Char, FString Buffer){CharacterDeleted.Broadcast(Char, Buffer); });
	DasherScreen->MouseListeners.BindLambda([this](int index, bool pressed) { MouseEvent.Broadcast(index, pressed); });
	DasherScreen->BoostListeners.BindLambda([this](int index, bool pressed) { BoostEvent.Broadcast(index, pressed); });
	DasherScreen->BufferAltered.BindLambda([this](FString Buffer) { BufferAltered.Broadcast(Buffer); });
	DasherScreen->CharacterEnteredLastFrame.BindLambda([this](){	CharacterEnteredLastFrame.Broadcast();	});
	DasherScreen->CharacterDeletedLastFrame.BindLambda([this](){	CharacterDeletedLastFrame.Broadcast();	});
	DasherScreen->CharacterSwitchedLastFrame.BindLambda([this](){	CharacterSwitchedLastFrame.Broadcast();	});
	DasherScreen->DasherFrameCompleted.BindLambda([this]() { DasherFrameCompleted.Broadcast(); });
}

void UDasherWidget::ReleaseSlateResources(bool bReleaseChildren)
{
	Super::ReleaseSlateResources(bReleaseChildren);

	if(bReleaseChildren) DasherScreen.Reset();
}

FString UDasherWidget::GetBuffer()
{
	if(!DasherScreen) return "";
	return DasherScreen->GetBuffer();
}

void UDasherWidget::ResetBuffer()
{
	if(!DasherScreen) return;
	DasherScreen->ResetBuffer();
	BufferAltered.Broadcast("");
}

void UDasherWidget::StartTraining(FString PathToTextFile)
{
	if(!DasherScreen) return;
	DasherScreen->StartTraining(PathToTextFile);
}

void UDasherWidget::SetBoolParamter(FString ParameterName, bool Value)
{
	if(!DasherScreen) return;
	DasherScreen->SetParameter(ParameterName, Value);
}

void UDasherWidget::SetLongParamter(FString ParameterName, int64 Value)
{
	if(!DasherScreen) return;
	DasherScreen->SetParameter(ParameterName, Value);
}

void UDasherWidget::SetStringParamter(FString ParameterName, FString Value)
{
	if(!DasherScreen) return;
	DasherScreen->SetParameter(ParameterName, Value);
}

void UDasherWidget::InputButton(bool Pressed)
{
	if(!DasherScreen) return;
	DasherScreen->VectorInputButton(Pressed);
}

void UDasherWidget::InputVector(FVector2D InputVector)
{
	if(!DasherScreen) return;
  DasherScreen->InputVector(InputVector);
}

bool UDasherWidget::GetInputEnabled() const
{
	if(!DasherScreen) return false;
	return DasherScreen->GetInputEnabled();
}

void UDasherWidget::EnableInput(bool enabled)
{
	if(!DasherScreen) return;
	DasherScreen->EnableInput(enabled);
}

#if WITH_EDITOR
const FText UDasherWidget::GetPaletteCategory()
{
  return LOCTEXT("Common", "Common");
}

#endif


/////////////////////////////////////////////////////

#undef LOCTEXT_NAMESPACE
