// Copyright Epic Games, Inc. All Rights Reserved.

#include "DasherVR.h"
#include "Engine.h"
#define LOCTEXT_NAMESPACE "FDasherVRModule"

void FDasherVRModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	//Currently nothing has to be done here
}

void FDasherVRModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FDasherVRModule, DasherVR)