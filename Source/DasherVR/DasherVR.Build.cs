// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DasherVR : ModuleRules
{
	public DasherVR(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		CppStandard = CppStandardVersion.Cpp17;
		PrivatePCHHeaderFile = "Private/DasherVRPrivatePCH.h";
		PrivateDefinitions.Add("HAVE_OWN_FILEUTILS");

		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);

		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
			}
			);
			
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"Dasher",
				"InputCore",
				"DeveloperSettings"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"UMG",
				"RenderCore"
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
