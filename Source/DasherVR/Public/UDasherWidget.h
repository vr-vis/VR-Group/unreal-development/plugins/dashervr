#pragma once

#include "CoreMinimal.h"

//~~~~~~~~~~~~ UMG ~~~~~~~~~~~~~~~~
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//Custom Slate Element
#include "SDasherWidget.h"
#include "UDasherWidget.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCharManipulatedEvent, FString, Char, FString, Buffer);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBufferManipulatedEvent, FString, Buffer);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FMouseEvent, int, PointerIndex, bool, IsMouseDown);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDasherEvent);

UCLASS(BlueprintType)
class DASHERVR_API UDasherWidget : public UWidget
{
	GENERATED_UCLASS_BODY()

private:
	//Custom Slate Element 
	TSharedPtr<SDasherWidget> DasherScreen;

public:
	//Functions to get and reset the Dasher Text Buffer
	UFUNCTION(BlueprintPure) FString GetBuffer();

	UFUNCTION(BlueprintCallable) void ResetBuffer();

	UFUNCTION(BlueprintCallable) void StartTraining(FString PathToTextFile);

	UFUNCTION(BlueprintCallable) void SetBoolParamter(FString ParameterName, bool Value);
	UFUNCTION(BlueprintCallable) void SetLongParamter(FString ParameterName, int64 Value);
	UFUNCTION(BlueprintCallable) void SetStringParamter(FString ParameterName, FString Value);

	UFUNCTION(BlueprintCallable) void InputButton(bool Pressed);
	UFUNCTION(BlueprintCallable) void InputVector(FVector2D InputVector);

	UFUNCTION(BlueprintPure) bool GetInputEnabled() const;
	UFUNCTION(BlueprintCallable) void EnableInput(bool enabled);

	UPROPERTY(BlueprintAssignable) FCharManipulatedEvent CharacterEntered;
	UPROPERTY(BlueprintAssignable) FCharManipulatedEvent CharacterDeleted;
	UPROPERTY(BlueprintAssignable) FCharManipulatedEvent CharacterSwitched;
	UPROPERTY(BlueprintAssignable) FBufferManipulatedEvent BufferAltered;
	UPROPERTY(BlueprintAssignable) FMouseEvent MouseEvent;
	UPROPERTY(BlueprintAssignable) FMouseEvent BoostEvent;
	UPROPERTY(BlueprintAssignable) FDasherEvent DasherFrameCompleted;
	UPROPERTY(BlueprintAssignable) FDasherEvent CharacterSwitchedLastFrame;
	UPROPERTY(BlueprintAssignable) FDasherEvent CharacterEnteredLastFrame;
	UPROPERTY(BlueprintAssignable) FDasherEvent CharacterDeletedLastFrame;
public:

	// UWidget interface
	virtual void SynchronizeProperties() override;
	// End of UWidget interface

	// UVisual interface
	virtual void ReleaseSlateResources(bool bReleaseChildren) override;
	// End of UVisual interface

#if WITH_EDITOR
	// UWidget interface
	//virtual const FSlateBrush* GetEditorIcon() override;
	virtual const FText GetPaletteCategory() override;
	// End UWidget interface
#endif

protected:
	// UWidget interface
	virtual TSharedRef<SWidget> RebuildWidget() override;
	// End of UWidget interface
	
};