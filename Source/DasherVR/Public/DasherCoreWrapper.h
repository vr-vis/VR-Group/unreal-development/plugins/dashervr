#pragma once

//Includes wrapped for all the DasherCore functionality used in the plugin


#pragma warning(push)

//clang specific warnings
#if PLATFORM_LINUX
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Woverloaded-virtual"
#pragma clang diagnostic ignored "-Wdefaulted-function-deleted"
#endif

	THIRD_PARTY_INCLUDES_START
	//included first to stop linker errors
	#include "DasherCore/SettingsStore.h"
	#include "DasherCore/AbstractXMLParser.h"
	#include "DasherCore/DasherInterfaceBase.h"
	#include "DasherCore/FileUtils.h"
	#include "DasherCore/DasherScreen.h"
	#include "DasherCore/DashIntfScreenMsgs.h"	
	#include "DasherCore/DasherInput.h"
	#include "DasherCore/UserLog.h"
	#include "DasherCore/XmlSettingsStore.h"
	#include "DasherCore/Messages.h"
	#include "DasherCore/ModuleManager.h"	
	#include "Common/AppSettingsHeader.h"
	THIRD_PARTY_INCLUDES_END
#if PLATFORM_LINUX
#pragma clang diagnostic push
#endif
#pragma warning(pop)