#pragma once
#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "DasherCoreWrapper.h"
#include <utility>

#include "DasherInterface.h"
#include "Math/Vector2D.h"
#include "Fonts/FontMeasure.h"
#include "Widgets/DeclarativeSyntaxSupport.h"

//using namespace Dasher;

//Structs to hold the elements making up the UI

enum class GeometryType : uint8
{

	Rectangle = 0,
	Writing = 1,
	PolyLine = 2
};

enum class DasherEventType : uint8
{
	Input_MouseDown = 1,
	Input_MouseUp = 2,
	Tick = 3
};

struct DasherDrawGeometry
{
	GeometryType Type;
	DasherDrawGeometry(GeometryType Type) : Type(Type) {}
	virtual ~DasherDrawGeometry() {};
};

struct FFilledRect : DasherDrawGeometry {
	FVector2D top;
	FVector2D bottom;
	FLinearColor color;

	FFilledRect(FVector2D Top, FVector2D Bottom, FLinearColor Color) : DasherDrawGeometry(GeometryType::Rectangle), top(Top), bottom(Bottom), color(Color) {}
};

struct FWriting : DasherDrawGeometry{
	FString label;
	FVector2D pos;
	int size;
	FLinearColor color;
	FWriting(FString Label, FVector2D Pos, int Size, FLinearColor Color) : DasherDrawGeometry(GeometryType::Writing), label(Label), pos(Pos), size(Size), color(Color)  {}
};

struct FPolyLine : DasherDrawGeometry{
	TArray<FVector2D> points;
	float linewidth;
	bool AntiAliasing;
	FLinearColor color;

	FPolyLine(TArray<FVector2D> Points, float LineWidth, FLinearColor Color, bool AntiAliasing): DasherDrawGeometry(GeometryType::PolyLine), points(Points), linewidth(LineWidth), AntiAliasing(AntiAliasing), color(Color)  {}
};

struct FMouseState
{
	bool PrimaryButtonPressed = false;
	bool SecondaryButtonPressed = false;
	bool DisabledDueToMouseLeave = false;
	FVector2D CursorPosition = {0,0};
	bool CurrentlyHovering = true;
};

DECLARE_DELEGATE(FDasherEventDelegate);
DECLARE_DELEGATE_TwoParams(FDasherMouseDelegate, int, bool);
DECLARE_DELEGATE_OneParam(FBufferManiputlationDelegate, FString);
DECLARE_DELEGATE_TwoParams(FCharManiputlationDelegate, FString, FString);


class DASHERVR_API SDasherWidget : public SCompoundWidget, public Dasher::CDasherScreen, public Dasher::CScreenCoordInput
{
public:


	SLATE_BEGIN_ARGS(SDasherWidget)
	{}
	
	SLATE_ARGUMENT(int, height);
	SLATE_ARGUMENT(int, width);
	SLATE_END_ARGS()

	

	typedef Dasher::screenint screenint;
	
	SDasherWidget(): CDasherScreen(0,0), CScreenCoordInput("Mouse Input") {}

	// Constructs this widget with InArgs. Needed for every widget. Builds this widget and any of its children
	void Construct(const FArguments& InArgs);
	void SetParameter(FString& ParameterName, bool Value);
	void SetParameter(FString& ParameterName, int64 Value);
	void SetParameter(FString& ParameterName, FString Value);

	virtual int32 OnPaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyClippingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;

	//set of functions inherited from Dasherscreen used for displaying, see DasherScreen for further info

    virtual std::pair<screenint, screenint> TextSize(CDasherScreen::Label* Label, unsigned int Size) override;

	virtual void DrawString(CDasherScreen::Label *label, screenint x1, screenint y1, unsigned int size, const Dasher::ColorPalette::Color& color) override;

	virtual void DrawRectangle(Dasher::screenint x1, Dasher::screenint y1, Dasher::screenint x2, Dasher::screenint y2, const Dasher::ColorPalette::Color& color, const Dasher::ColorPalette::Color& outlinecolor, int ithickness) override;

	virtual void DrawCircle(screenint icx, screenint icy, screenint ir, const Dasher::ColorPalette::Color& ifillcolor, const Dasher::ColorPalette::Color& ilinecolor, int ithickness) override {} //we don't really need to draw circles, so it's not implemented

	virtual void Polyline(CDasherScreen::point* points, int number, int iwidth, const Dasher::ColorPalette::Color& color) override;

	virtual void Polygon(CDasherScreen::point* points, int number, const Dasher::ColorPalette::Color& fillcolor, const Dasher::ColorPalette::Color& outlinecolor, int iwidth) override;

	virtual void Display() override;

	virtual bool IsPointVisible(screenint x, screenint y) override { return true; }

	//Pass-me-down returning Buffer
	FString GetBuffer() const;
	void ResetBuffer();

	void StartTraining(FString PathToTextFile);

	//Tick function inherited from SCompoundWidget
	virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

	//Function to set if the widget is in editor or not
	void SetEditor(bool EditorState);

	//mouse handling function
	virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
    virtual FReply OnMouseMove(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
    virtual void OnMouseEnter(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
    virtual void OnMouseLeave(const FPointerEvent& MouseEvent) override;
    virtual FReply OnMouseButtonDoubleClick(const FGeometry& InMyGeometry, const FPointerEvent& InMouseEvent) override;
	virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

	virtual bool SupportsKeyboardFocus() const override {return true;}

	virtual bool GetScreenCoords(screenint& iX, screenint& iY, Dasher::CDasherView* pView) override;

	void InputVector(FVector2D InputVector);
	void VectorInputButton(bool Pressed);
	void VectorBoostButton(bool Pressed);
	FVector2D GetCursorPosition() const;

	//Allows to Pause Input
	void EnableInput(bool enable);
	bool GetInputEnabled() const;
	FBufferManiputlationDelegate CharacterSwitched;
	FDasherMouseDelegate MouseListeners;
	FDasherMouseDelegate BoostListeners;
	FCharManiputlationDelegate CharacterEntered;
	FCharManiputlationDelegate CharacterDeleted;
	FDasherEventDelegate CharacterEnteredLastFrame;
	FDasherEventDelegate CharacterDeletedLastFrame;
	FDasherEventDelegate CharacterSwitchedLastFrame;
	FBufferManiputlationDelegate BufferAltered;
	FDasherEventDelegate DasherFrameCompleted;

private:
	
	//Buffers to store geometry that is drawn in OnPaint
	TArray<TUniquePtr<DasherDrawGeometry>> GeometryBufferA;
	TArray<TUniquePtr<DasherDrawGeometry>> GeometryBufferB;
	TArray<TUniquePtr<DasherDrawGeometry>>* BackBuffer = &GeometryBufferA;
	TArray<TUniquePtr<DasherDrawGeometry>>* FrontBuffer = &GeometryBufferB;
	
	int Height = 0;
	int Width = 0;

	bool CurrentlyUsingVectorInput = false;
	point CursorPosition = {0,0};
	point NewMousePosition = {0,0};

	bool IsEditor = true; //are we in the Editor?
	bool CharDeletedInFrame = false;
	bool CharEnteredInFrame = false;
	bool InputPaused = false; //is the input paused

	//Mouse States
    TMap<int, FMouseState> MouseStates;
	TArray<int> MouseInteractions;
	const int VectorInputPointerIndex = 42;
	FMouseState& GetMouseState(int Index);
    void UpdateMouseInteraction(int Index);
    int GetLastActiveMouseInteraction() const;

	//set up the font measure service to ... measure fonts.
	TSharedPtr<FSlateFontMeasure> FontMeasureService;

protected:
	// stores color information 
	TSharedPtr<Dasher::DasherInterface> DasherMainInterface;
};

