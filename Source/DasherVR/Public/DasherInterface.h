#pragma once

#include <functional>

#include "CoreMinimal.h"
#include "DasherCoreWrapper.h"

class SDasherWidget;

namespace Dasher
{

	//Just a function to Log XML errors
	class XMLErrorDisplay : public CMessageDisplay
	{
	public:
		void Message(const std::string& strText, bool bInterrupt) override
		{
			UE_LOG(LogTemp, Log, TEXT("%s"), UTF8_TO_TCHAR(strText.c_str()));
		}
	};

	//Implementation of abstract class CDashIntfScreenMsgs
	class  DasherInterface : public CDashIntfScreenMsgs
	{
	public:

		//Might need empty constructor+ init method
		DasherInterface(CSettingsStore* settings);
		~DasherInterface(void) {}
		/*void Main();*/

		//Subclasses should return the contents of (the specified subrange of) the edit buffer
		virtual std::string GetContext(unsigned int iStart, unsigned int iLength) override;

        /// Subclasses should return character, word, sentence, ... at current text cursor position.
		/// For character around cursor decision is arbitrary. Let's settle for character before cursor.
		/// TODO. Consistently name functions dealing with dasher context, versus functions dealing with editor text.
		/// I.E. GetAllContext should be named GetAllTtext
		virtual std::string GetTextAroundCursor(Dasher::EditDistance) override;

		
		///Called to execute a control-mode "move" command.
		///\param bForwards true to move forwards (right), false for backwards
		///\param dist how far to move: character, word, line, file. (Usually defined
		/// by OS, e.g. for non-european languages)
		///\return the offset, into the edit buffer of the cursor *after* the move.
		virtual unsigned int ctrlMove(bool bForwards, Dasher::EditDistance dist) override;

		
		///Called to execute a control-mode "delete" command.
		///\param bForwards true to delete forwards (right), false for backwards
		///\param dist how much to delete: character, word, line, file. (Usually defined
		/// by OS, e.g. for non-european languages)
		///\return the offset, into the edit buffer, of the cursor *after* the delete
		/// (for forwards deletion, this will be the same as the offset *before
		virtual unsigned int ctrlDelete(bool bForwards, Dasher::EditDistance dist) override;
		
		///Clears all written text from edit buffer and rebuilds the model. The default
		/// implementation does this using the control mode editDelete mechanism
		/// (one call forward, one back), followed by a call to SetBuffer(0). Subclasses
		/// may (optionally) override with more efficient / easier implementations, but
		/// should make the same call to SetBuffer.
		virtual std::string GetAllContext() override;

		///// Seems to be the only way to delete a symbol
		virtual void editDelete(const std::string& strText, CDasherNode* pNode) override;

		virtual void editOutput(const std::string& strText, CDasherNode* pNode) override;

		/// Subclasses should return the length of whole text. In letters, not bytes.
		virtual int GetAllContextLenght() override { return Buffer.Len(); }
		
		//tick function 
		void Tick(unsigned long time);
		
		// set the screen, needed for maximum spaghetti, to make changeScreen accessible to the outside aka SDasherWidget
		virtual void SetScreen(CDasherScreen* screen);
		//Sets the Training Filename
		virtual void ImportTrainingFile(std::string filename);

		virtual FVector2D ConvertDasher2Screen(myint InputX, myint InputY);

		//return Buffer
		FString GetBuffer() const;
		//delete Buffer
		void ResetBuffer();

		void SetCharEnteredCallback(std::function<void(FString, FString)> Callback)
		{
			CharacterEntered = Callback;
		}

		void SetCharDeletedCallback(std::function<void(FString, FString)> Callback)
		{
			CharacterDeleted = Callback;
		}

	private:
		//Cursor position in the output buffer
		int Cursor = 0;
		//Output Buffer
		FString Buffer = "";
		//Dasher Settings Object
		CSettingsStore* settings;

		//Callbacks
		std::function<void(FString, FString)> CharacterEntered = nullptr;
		std::function<void(FString, FString)> CharacterDeleted = nullptr;  
	};

}